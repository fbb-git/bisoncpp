inline bool Block::assignment() const
{
    return d_assignment;
}

inline void Block::operator+=(std::string const &text)
{
    append(text);
}

inline Block::operator bool() const
{
    return d_count;
}

inline std::vector<AtDollar>::const_reverse_iterator 
Block::rbeginAtDollar() const
{
    return d_atDollar.rbegin();
}

inline std::vector<AtDollar>::const_reverse_iterator 
Block::rendAtDollar() const
{
    return d_atDollar.rend();
}

inline size_t Block::lineNr() const
{
    return d_lineNr;
}

inline void Block::setLineNr(size_t lineNr)
{
    d_lineNr = lineNr;
}

inline std::string const &Block::source() const
{
    return d_source;
}

inline std::string const &Block::str() const
{
    return *this;
}
