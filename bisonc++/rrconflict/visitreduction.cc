#include "rrconflict.ih"

void RRConflict::visitReduction(size_t first)
{
    d_firstIdx = d_reducible[first];
    d_firstLA = &d_itemVector[d_firstIdx].lookaheadSet();

    size_t rrNr = 0;            // set to d_itemVector[d_firstIdx].nr()
                                // at a conflict in compareReuctions

                            // this MUST be second < d_reducible.size() !!
    for (size_t second = first + 1; second < d_reducible.size(); ++second)
        compareReductions(&rrNr, second);
}
