inline int State::type() const
{
    return d_stateType.type();
}

inline size_t State::idx() const
{
    return d_idx;
}

inline size_t State::nStates() 
{
    return s_state.size();
}

inline size_t State::terminalTransitions() const
{
    return d_nTerminalTransitions;
}

inline size_t State::transitions() const
{
    return d_nTransitions;
}

inline StateItem const *State::reduction(size_t idx) const
{
    return idx >= d_reducible.size() ? 
                0
            :
                &d_itemVector[d_reducible[idx]];
}

inline size_t State::defaultReduction() const
{
    return d_defaultReducible;
}

inline size_t State::reductions() const
{
    return d_nReductions;
}

inline size_t State::reductionsLAsize() const
{
    return d_summedLAsize;
}

inline State::ConstIter State::begin()
{
    return s_state.begin();
}

inline State::ConstIter State::end()
{
    return s_state.end();
}

inline Next::Vector const &State::next() const
{
    return d_nextVector;
}

inline bool State::isAcceptState() const
{
    return this == s_acceptState;
}

inline bool State::nextContains(Next::ConstIter *iter, 
                                Symbol const *symbol) const
{
    return (*iter = nextFind(symbol)) != d_nextVector.end();
}

inline void State::showSRConflicts(Rules const &rules) const
{
    d_srConflict.showConflicts(rules);
}

inline void State::showRRConflicts(Rules const &rules) const
{
    d_rrConflict.showConflicts(rules);
}

inline std::ostream &operator<<(std::ostream &out, State const *state)
{
    return (state->*State::s_insert)(out);
    // One of: insertStd, insertExt or skipInsertion.
}
