#include "generator.ih"

namespace 
{
    char const endPolyTypeAssign[] =
R"(inline SType &SType::operator=(EndPolyType_ const &value)
{
    assign< Tag_::END_TAG_ >(value);
    return *this;
}
inline SType &SType::operator=(EndPolyType_ &&tmp)
{
    assign< Tag_::END_TAG_ >(std::move(tmp));
    return *this;
}
)";

}
void Generator::polymorphicOpAssignImpl(ostream &out) const
{
    key(out);

    for (auto &poly: d_polymorphic)
        out << 
            "inline SType &SType::operator=(" << poly.second << 
                                                            " const &value)\n"
            "{\n"
            "    assign< Tag_::" << poly.first << " >(value);\n"
            "    return *this;\n"
            "}\n"
            "inline SType &SType::operator=(" << poly.second << " &&tmp)\n"
            "{\n"
            "    assign< Tag_::" << poly.first << " >(std::move(tmp));\n"
            "    return *this;\n"
            "}\n";

    out << endPolyTypeAssign;
}



